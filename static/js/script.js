var station,system;
var stationNameMsg = "Enter Station or System without spaces";

var askNewStation = function(){
    var s = prompt(stationNameMsg);
    if(s != "null" && s != null)
        $.bbq.pushState({station:s});
}  
  
var toggleDark = function(){
    if(localStorage.getItem("darkPref") == "true"){
        localStorage.setItem("darkPref",false);
        $("body").removeClass("oj");
    }
    else{
        localStorage.setItem("darkPref",true)
        $("body").addClass("oj");                
    }
}

var loadData = function(noMessage){
    clearTimeout(serviceRequest);
    $hangingRequest && $hangingRequest.abort && $hangingRequest.abort();
    $(".progressHeader, .progress").remove();
    serviceRequest = setTimeout(function(){
        $hangingRequest = pricesRequest(noMessage);
    },100);
}

var pricesRequest = function(noMessage){
    var params = $.deparam.fragment();
    var askTime = new Date().getTime();
    if(typeof(params.station) != "undefined"){
        $("#saveData").hide();
        $("#tableHolder").height($("#tableHolder").height());        
        if(!noMessage || lastUpdateStation != params.station){
            $("#tableHolder").html(ich.loadingSlug({msg:"Loading commodity values",time:lastUpdateTook}));
            $(".progress .progress-bar").progressbar();
        }
        return $.getJSON(("/prices/?station="+params.station+"&s="+new Date()),function(res){
            lastUpdatedStation = params.station;
            lastUpdateTook = new Date().getTime() - askTime;
            $(res.categories).each(function(i,cat){
                cat.catId = cat.category.replace(/ /g,"");
                cat.items = decorateData(cat.items,cat.catId);
            });
            if(res.modified != "" && res.modified != null){
                res.modified = res.modified.substring(5);
            }            
            $("#tableHolder").html(ich.commodityTable(res));
            station = res.station;
            system = res.system
//            $.bbq.pushState({mode:"update"});
            document.title = res.station + " - Trade Dangerous";
            setTimeout(function(){
                $("table").stickyTableHeaders();
                $("tr.categoryLabel").each(function(){
                    $(this).toggle($("tr.cat" + $(this).data("cat") + ":visible").length > 1)
                });
            },200);
        }).error(function(e){
            ich.commodityTable({system:"Unable to find system"});        
//            setTimeout(askNewStation,500);
        });
    }
/*    else{
        askNewStation();
    }*/
};

var itemLevelConst = function(level) {
    if(parseInt(level) > -2){
        switch(level){
            case 0:return "";
                break;
            case 1:return "L";
                break;
            case 2:return "M";
                break;
            case 3:return "H";
                break;
            default:
                return "";
                break;
        }
    }
    else{
        return level;
    }
}
    
var decorateData = function(lines,catId){
    $(lines).each(function(i,line){
        lines[i].catId = catId;
        lines[i].stock = (line.stock > -1) ? line.stock : "?";
        var os = line.stockLevel;
        lines[i].stockLevel = itemLevelConst(lines[i].stockLevel);
        switch(os){
            case 0: lines[i].stockEmpty = true;
                break;        
            case 1: lines[i].stockLow = true;
                break;
            case 2: lines[i].stockMed = true;
                break;
            case 3: lines[i].stockHigh = true;
                break;
            default: lines[i].stockUnknown = true;
                break;
        }
        lines[i].demand = (line.demand > -1) ? line.demand : "?";        
        var od = lines[i].demandLevel;
        lines[i].demandLevel = itemLevelConst(lines[i].demandLevel);                
        switch(od){
            case 0: lines[i].stockEmpty = true;
                break;        
            case 1: lines[i].demandLow = true;
                break;
            case 2: lines[i].demandMed = true;
                break;
            case 3: lines[i].demandHigh = true;
                break;
            default: lines[i].demandUnknown = true;
                break;
        }
        if(line.fromStn == 0 && line.fromStn == 0){
            line.noPricing = true;
        }
    });    
    return lines;
}
        
var getWallOfPrices = function(){
    var lines = ["@ "+system+"/"+station];
//    var now = new Date(); //matching this date format: 2014-10-20 06:53:06 
//    now = now.getFullYear() +"-"+ (now.getMonth()+1) +"-"+ now.getDate() +" "+ now.getUTCHours() +":"+ now.getMinutes()+":"+now.getSeconds();
    
    function getCellVal(cell){
        var $input = $("input",cell);
        var val = ($input.val() != "") ? $input.val() : $input.attr("placeholder");
        return val || "?";
    }
    
    $("#priceTable tbody tr").each(function(i,row){
        if($(row).hasClass("categoryLabel")){
            lines.push("+ " + $("td:first-child",row).text());
        }
        else{
            var tds = $("td",row);
            var line = []
            line.push($(tds[0]).text());
            line.push(getCellVal(tds[1]));
            line.push(getCellVal(tds[2]));                
            line.push(supply(getCellVal(tds[3]),itemLevelConst(getCellVal(tds[4]))));
            line.push(supply(getCellVal(tds[5]),itemLevelConst(getCellVal(tds[6]))));
//            line.push("now");
            lines.push(line.join(" "));
        }
    });
    
    function supply(quantity,level){
        if(quantity + level == "??"){
            return "unk";
        }
        else if(level == "?"){
            return quantity; 
        }
        else if(quantity == "?"){
            return "0" + level;
        }
        else{
            return quantity+level;
        }
        
    }
    return lines.join("\n");
}

var savePriceLines = function(){
    $.ajax({
        url:"/savePriceLines/",
        type:"POST",
        data:{
            station:station,
            lines:getWallOfPrices()
        },
        success:function(res){
            $.bbq.pushState({mode:"update"});
            loadData(true);
        },
        error:function(e){
            console.log("There was an issue POSTing the wall of prices");
            console.log(e);
        }
    });        
}

var saveJSON = function(){
    $.ajax({
        url:"/savePrices/",
        type:"POST",
        data:getChanges(),
        contentType:"application/json",
        success:function(res){
            $.bbq.pushState({mode:"update"});
        },
        error:function(e){
            console.log("There was an issue POSTing the JSON slug");
            console.log(e);
        }
    });
}
    
var getChanges = function(){
    var response = {
        station:station,
        changes:[],
        stamp:new Date()
    };
    $("tbody tr").not(".categoryLabel").each(function(i,row){
        var change = {
            item:$("td:first-child",row).text()
        }                
        if($("input",row).filter(function(){ return !!this.value; }).length > 0){
            $("input",row).each(function(j,input){
                $input = $(input);
                change[$input.data("field")] = ($input.val() != "") ? $input.val() : $input.attr("placeholder");
            });
            response.changes.push(change);
        }
    });
    return JSON.stringify(response);
}        

var getStationId = function(s){
    return s.replace(/[^a-zA-Z0-9]+/g,"")
}

String.prototype.commafy = function(){
    return this.replace(/(^|[^\w.])(\d{4,})/g,function($0, $1, $2){
        return $1 + $2.replace(/\d(?=(?:\d\d\d)+(?!\d))/g, "$&,");
    });
};


var unbindChecklist = function(){
    $("#routeput a.list-group-item").attr("href","javascript:void(0);");
    $("#routeput a.list-group-item").click(function(e){
        $clicked = $(this);
        var top = $clicked.index();
        var lastDocked = "";
        $("a.list-group-item").each(function(i,link){
            if(i <=top){
                $(link).addClass("disabled");
                if($(link).text().indexOf("Dock at ") == 0){
                    lastDocked = getStationId($(link).text().substring(8));
                }
            }
            else
                $(link).removeClass("disabled");
        });
        $("#routeput a.list-group-item").css("fontSize","1em");
        $clicked.next("a").css("fontSize","3em");
        var stats = calcRouteRate();
        if(lastDocked != "")
            $.bbq.pushState({station:lastDocked});
        $("#rate").text("Current "+stats.time.mins+" minute trade run is netting " + stats.total +"cr or " + stats.crPerHr + "cr per hour with "+stats.crInHold+"cr in cargo.").show();
        setTimeout(function(){
            $(window).scrollTop($clicked.position().top)
        },200);
    });
    $("#routeput a.list-group-item:first-child").css("fontSize","3em");
    var lastStation = $(".jumbotron a:last").text()
    var lastStationId = getStationId(lastStation);
    $("#routeput").append("<a href='#station="+lastStationId+"&mode=run' onclick='setTimeout(function(){getRoute(1)},100)' class='btn btn-primary btn-lg' role='button'>New from "+lastStation+"</a>");
    $("#routeput a.list-group-item").each(function(i,item){
        if(getLineValue($(item).text()) != 0){
            $(item).append("<a class='bbq' href='#update' data-h='mode'><span class='glyphicon glyphicon-edit'></span></a>");
        }
    });
    $(window).scrollTop($("#routeput").position().top+80);            
}        

var calcRouteRate = function(){
    var totalCr=0
    var crInHold = 0;
    var $items = $(".list-group-item.disabled");
    var lastOne = 0
    $items.each(function(i,item){
        val = getLineValue($(item).text());
        totalCr += val;
        if(val != 0)
            lastOne = val;
    });
    
    if($items.length != $(".list-group-item").length && lastOne < 0){
        totalCr -= lastOne;
        crInHold = -lastOne;                
    }

    //kinda ugly but badass to not floor for this
    var time = {};
    var s = (new Date().getTime() - routeStartTime);
    time.ms = s % 100;
    s = (s - time.ms) / 1000;
    time.secs = s % 60;
    s = (s - time.secs) / 60;
    time.mins = s % 60;
    
    return {
        crPerHr:(Math.floor(totalCr/time.secs)*60*60 + "").commafy(),
        total:(totalCr + "").commafy(),
        time:time,
        crInHold:(crInHold + "").commafy()
    }

}
var getLineValue = function(line){
    //plain old js choppin strings like a champ
    var s = line.indexOf("@") + 2;
    if(s > 2){
        var v = line.substring(0,3);
        var ct = line.split(" ")[1];
        var cr = parseInt(line.substring(s,line.length-2).replace(/,/g,"")) * ct;
        return (v == "Buy") ? -cr : cr;
    }
    else
        return 0;                
}

var getRoute = function(fromClick){
    clearTimeout(serviceRequest);
    $hangingRequest && $hangingRequest.abort && $hangingRequest.abort();
    $(".progressHeader, .progress").remove();    
    serviceRequest = setTimeout(function(){
        $hangingRequest = routeRequest(fromClick);
    },100);
}
var routeRequest = function(fromClick){
    var q = "run --webOutput";
    if($("#routes").val() == ""){
        q+= " --checklist";
        var multiRoute = false;
    }
    else{
        var multiRoute = true;
    }
    if(fromClick && $(".jumbotron").length > 0){
        $.bbq.pushState({station:getStationId($("#fromField").val())});
        $(window).trigger("hashchange");
//        return false;
    }
    lastFrom = params.station;
    if($("textarea:visible").length == 1){
        q = $("textarea").val();
    }
    else{
        $("#ship input.required").each(function(i,input){
            if($(this).val() == ""){
                $(this).addClass("missing"); //colorblind people are inferior
                $(this).attr("placeholder",$(this).attr("placeholder")+" (Required)");
            }
            else{
                $(this).removeClass("missing");
            }
            $("#ship input.missing:first").focus();
        });
        if($("input.missing").length > 0){
            $.bbq.pushState({mode:"ship"});
            return false;
        } 
        $("#builder input, #ship input").each(function(i,input){
            if($(this).val() != "")
                addArg($(this).data("arg"),$(this).hasClass("station") ? getStationId($(this).val()) : $(this).val());
            else
                localStorage.setItem($(this).data("arg"),"")    
        });
        $("textarea").val(q);
    }

    function addArg(arg,val){
        q += " " +  arg + val;
        localStorage.setItem("run."+arg,val);
    }
    
/*    if(localStorage.getItem("lastRun") == q){
        return false;
    }*/
    localStorage.setItem("lastRun",q);

    $("#routeput").html(ich.loadingSlug({msg:"Calculating route",time:lastRunTook}));
    $(".progress .progress-bar").progressbar();
    $(window).scrollTop($("#routeput").position().top);
    var routeAskTime = new Date().getTime();
    return $.ajax({
        url:"/doRun/",
        method:"POST",
        data:{
            command:q
        },
        success:function(res){
            //I'm kinda hating you&me at the moment for not doing a JSON structure to start with...
            if(!multiRoute){
                var endOfRoute = res.indexOf("total<br>");
                var route = res.substring(0,endOfRoute > -1 ? endOfRoute + 9 : endOfRoute=res.length );
                var routeLines = route.split("<br>");
                $(routeLines).each(function(i,line){
                    if(i > 0 && i != routeLines.length-1){
                                                var s = this.indexOf("/");
                        var g = this.indexOf("gaining",s);
                        var e = this.indexOf(",",s);
                        e = (g > 0) ? g : e;
                        var station = this.substring(s+1,e);                        
                        routeLines[i] = this.substring(0,s) + " / <a href='/#station="+getStationId(station)+"&mode=update'>"+station+"</a>"+this.substring(e);

                    }
                    else if(i == 0){
                        routeLines[0] = "<h3>"+line.replace(/\//g," / ")+"</h3>";
                    }
                });
            }
            else{
                var routeLines = res.split("total");
                console.log(res);
                console.log(routeLines)
            }
            if(multiRoute){
                routeLines = routeLines.join("</div><div>");
            }
            else{
                routeLines = routeLines.join("<br>");
            }
            routeLines = "<div class='jumbotron'>" + routeLines + "</div>" + res.substring(endOfRoute+9);
            $("#routeput").html(routeLines).css("marginBottom",$(".list-group").height());
            setTimeout(unbindChecklist,10);
            routeStartTime = new Date().getTime();
            lastRunTook = routeStartTime - routeAskTime;
        }
    });
}

            
var stationMatcher = function(stations) {
    return function findMatches(q, cb) {
        var matches, substrRegex;
        matches = [];
        substrRegex = new RegExp(q, 'i');
        $.each(stations, function(i, station) {
            if (substrRegex.test(station.label)) {
              matches.push(station);
            }
        }); 
        cb(matches);
    };
};

var swapStations = function(){
    var a = $("#fromField").val();
    var b = $("#toField").val();
    $("#fromField").val(b);
    $("#toField").val(a);
};


var markCol = function(el,col,val,cat){
    $("tr.cat"+cat+" td:nth-child("+col+")").children("input:visible").val(val);
    $("tr.cat"+cat+" td:nth-child("+col+") .picker:visible").find("a.level"+val).click();
}

var getNav = function(){
    var q = "nav " + getStationId($("#navFromField").val()) + " " + getStationId($("#navToField").val()) + " --webOutput --ly " +$("#shipLyField").val();
    $("#navOutput div").html(ich.loadingSlug({msg:"Calculating route",time:lastRunTook}));
    $(".progress .progress-bar").progressbar();
    
    $.ajax({
        url:"/doRun/",
        method:"POST",
        data:{
            command: q
        },
        success:function(res){
            $("#navOutput div").html("<table class='simpleTable'>" + res + "</table>");
            console.log(res);
        }
    });
}

var toggleEmptyRows = function(){
    $('tr.missingPricing').toggle();
    $("tr.categoryLabel").each(function(){
        $(this).toggle($("tr.cat" + $(this).data("cat")).length > 2)
    });
}


var getBuyOptions = function(){
    var buyQuantity = $("#buyQuantity").val();
    var near = getStationId($("#buyNear").val());
    var item = getStationId($("#buyItem").val());
    var q = "buy " + item + " " + ((near != "") ? " --near "+near : "") + " --webOutput --ly " +$("#shipLyField").val() + ((buyQuantity != "") ? " --quantity " + buyQuantity : "");
    
    if(item != ""){    
        $("#buyOutput div").html(ich.loadingSlug({msg:"Querying stations for "+item,time:lastBuyTook}));
        $(".progress .progress-bar").progressbar();
        var buyAskTime = new Date().getTime();
    
        $.ajax({
            url:"/doRun/",
            method:"POST",
            data:{
                command: q
            },
            success:function(res){
                lastBuyTook = new Date().getTime() - buyAskTime;                
                $("#buyOutput div").html("<table class='simpleTable'>" + res + "</table>");
            }
        });
    }
    else{
        $("#buyOutput div").html("<h3>Item is required</h3>");
    }
}

var lastUpdateTook = 3000;
var lastRunTook = 6300;
var lastBuyTook = 3000;
var lastUpdateStation = "";
var lastFrom = ""

//cheap debouncing
var serviceRequest = false;
var $hangingRequest = {};