#!/usr/bin/env python
#---------------------------------------------------------------------
# Copyright (C) Oliver 'kfsone' Smith 2014 <oliver@kfs.org>:
#  You are free to use, redistribute, or even print and eat a copy of
#  this software so long as you include this copyright notice.
#  I guarantee there is at least one bug neither of us knew about.
#---------------------------------------------------------------------
# TradeDangerous :: Command Line App :: Main Module
#
# TradeDangerous is a powerful set of tools for traders in Frontier
# Development's game "Elite: Dangerous". It's main function is
# calculating the most profitable trades between either individual
# stations or working out "profit runs".
#
# I wrote TD because I realized that the best trade run - in terms
# of the "average profit per stop" was rarely as simple as going
# Chango -> Dahan -> Chango.
#
# E:D's economy is complex; sometimes you can make the most profit
# by trading one item A->B and flying a second item B->A.
# But more often you need to fly multiple stations, especially since
# as you are making money different trade options are coming into
# your affordable range.
#
# END USERS: If you are a user looking to find out how to use TD,
# please consult the file "README.txt".
#
# DEVELOPERS: If you are a programmer who wants TD to do something
# cool, please see the TradeDB and TradeCalc modules. TD is designed
# to empower other programmers to do cool stuff.


######################################################################
# Imports

import argparse             # For parsing command line args.
import sys                  # Inevitably.
import time
import pathlib              # For path
import os
import math

######################################################################
# The thing I hate most about Python is the global lock. What kind
# of idiot puts globals in their programs?
import errno

args = None
originStation, finalStation = None, None
# Things not to do, places not to go, people not to see.
avoidItems, avoidSystems, avoidStations = [], [], []
# Stations we need to visit
viaStations = set()
originName, destName = "Any", "Any"
origins = []
maxUnits = 0

######################################################################
# Database and calculator modules.

from tradeexcept import TradeException
from tradedb import TradeDB, AmbiguityError
from tradecalc import Route, TradeCalc, localedNo

######################################################################
# webapp helpers
# replace with a real response object with arrays n shit
tradeOutput = ""
displayOutput = {"lines":[]}
def tradeMessage():
    return tradeOutput;

def newResponse():
    global displayOutput
    displayOutput = {"lines":[]}
    
def getResponse():
    global displayOutput
    return displayOutput
    
def addOutput(m="",mType=""):
    global tradeOutput
    global displayOutput
    tradeOutput = "<br>".join((tradeOutput + m).split("\n"))
    displayOutput["lines"].append({"msg":m,"mType":mType})

######################################################################
# Helpers

class CommandLineError(TradeException):
    """
        Raised when you provide invalid input on the command line.
        Attributes:
            errorstr       What to tell the user.
    """
    def __init__(self, errorStr):
        self.errorStr = errorStr
    def __str__(self):
        addOutput(str(self.errorStr))    
        return 'Error in command line: {}'.format(self.errorStr)


class NoDataError(TradeException):
    """
        Raised when a request is made for which no data can be found.
        Attributes:
            errorStr        Describe the problem to the user.
    """
    def __init__(self, errorStr):
        self.errorStr = errorStr
    def __str__(self):
        addOutput(self.errorStr)    
        return "Error: {}\n".format(self.errorStr) + \
        "This can happen if you have not yet entered any price data for the station(s) involved, " + \
            "if there are no profitable trades between them, " + \
            "or the items are marked as 'n/a'.\n" + \
        "See 'trade.py update -h' for help entering prices, or obtain a '.prices' file from the interwebs.\n" + \
        "Or see https://bitbucket.org/kfsone/tradedangerous/wiki/Price%20Data for more help.\n"


class HelpAction(argparse.Action):
    """
        argparse action helper for printing the argument usage,
        because Python 3.4's argparse is ever so subtly very broken.
    """
    def __call__(self, parser, namespace, values, option_string=None):
        parser.print_help()
        sys.exit(0)


class EditAction(argparse.Action):
    """
        argparse action that stores a value and also flags args._editing
    """
    def __call__(self, parser, namespace, values, option_string=None):
        setattr(namespace, "_editing", True)
        setattr(namespace, self.dest, values or self.default)


class EditActionStoreTrue(argparse.Action):
    """
        argparse action that stores True but also flags args._editing
    """
    def __init__(self, option_strings, dest, nargs=None, **kwargs):
        if nargs is not None:
            raise ValueError("nargs not allowed")
        super(EditActionStoreTrue, self).__init__(option_strings, dest, nargs=0, **kwargs)
    def __call__(self, parser, namespace, values, option_string=None):
        setattr(namespace, "_editing", True)
        setattr(namespace, self.dest, True)


def new_file_arg(string):
    """ argparse action handler for specifying a file that does not already exist. """

    path = pathlib.Path(string)
    if not path.exists(): return path
    sys.stderr.write("ERROR: Specified file, \"{}\", already exists.\n".format(path))
    sys.exit(errno.EEXIST)


class ParseArgument(object):
    """
        Provides argument forwarding so that 'makeSubParser' can take function-like arguments.
    """
    def __init__(self, *args, **kwargs):
        self.args, self.kwargs = args, kwargs


def makeSubParser(subparsers, name, help, commandFunc, arguments=None, switches=None, epilog=None):
    """
        Provide a normalized sub-parser for a specific command. This helps to
        make it easier to keep the command lines consistent and makes the calls
        to build them easier to write/read.
    """

    subParser = subparsers.add_parser(name, help=help, add_help=False, epilog=epilog)

    def addArguments(group, options, required, topGroup=None):
        """
            Registers a list of options to the specified group. Nodes
            are either an instance of ParseArgument or a list of
            ParseArguments. The list form is considered to be a
            mutually exclusive group of arguments.
        """

        for option in options:
            # lists indicate mutually exclusive subgroups
            if isinstance(option, list):
                addArguments((topGroup or group).add_mutually_exclusive_group(), option, required, topGroup=group)
            else:
                assert not required in option.kwargs
                if option.args[0][0] == '-':
                    group.add_argument(*(option.args), required=required, **(option.kwargs))
                else:
                    group.add_argument(*(option.args), **(option.kwargs))

    if arguments:
        argParser = subParser.add_argument_group('Required Arguments')
        addArguments(argParser, arguments, True)

    switchParser = subParser.add_argument_group('Optional Switches')
    switchParser.add_argument('-h', '--help', help='Show this help message and exit.', action=HelpAction, nargs=0)
    addArguments(switchParser, switches, False)

    subParser.set_defaults(proc=commandFunc)

    return subParser


def printHeading(text):
    """ Print a line of text followed by a matching line of '-'s. """
    print(text)
    print('-' * len(text))


######################################################################
# Checklist functions

class Checklist(object):
    def __init__(self, tdb, mfd):
        self.tdb = tdb
        self.mfd = mfd

    def doStep(self, action, detail=None, extra=None):
        self.stepNo += 1
        try:
            self.mfd.display("#{} {}".format(self.stepNo, action), detail or "", extra or "")
        except AttributeError: pass
        if not args.webOutput:
            input("   {:<3}: {}: ".format(self.stepNo, " ".join([item for item in [action, detail, extra] if item])))
        else:
            addOutput('<a href="#" class="list-group-item">{}</a>'.format(" ".join([item for item in [action, detail, extra] if item])))

    def note(self, str, addBreak=True):
        print("(i) {} (i){}".format(str, "\n" if addBreak else ""))

    def run(self, route, credits):
        tdb, mfd = self.tdb, self.mfd
        stations, hops, jumps = route.route, route.hops, route.jumps
        lastHopIdx = len(stations) - 1
        gainCr = 0
        self.stepNo = 0
        addOutput("<div class='list-group'>");
        printHeading("(i) BEGINNING CHECKLIST FOR {} (i)".format(route.str()))
        print()
        
        if args.detail:
            print(route.summary())
            print()

        for idx in range(lastHopIdx):
            hopNo = idx + 1
            cur, nxt, hop = stations[idx], stations[idx + 1], hops[idx]
            sortedTradeOptions = sorted(hop[0], key=lambda tradeOption: tradeOption[1] * tradeOption[0].gainCr, reverse=True)

            # Tell them what they need to buy.
            if args.detail:
                self.note("HOP {} of {}".format(hopNo, lastHopIdx))

            self.note("Buy at {}".format(cur.name()))
            for (trade, qty) in sortedTradeOptions:
                self.doStep('Buy {} x'.format(qty), trade.name(), '@ {}cr'.format(localedNo(trade.costCr)))
            if args.detail:
                self.doStep('Refuel')
            print()

            # If there is a next hop, describe how to get there.
            self.note("Fly {}".format(" -> ".join([ jump.name() for jump in jumps[idx] ])))
            if idx < len(hops) and jumps[idx]:
                for jump in jumps[idx][1:]:
                    self.doStep('Jump to', jump.name())
            if args.detail or args.webOutput:
                self.doStep('Dock at', nxt.str())
            print()

            self.note("Sell at {}".format(nxt.name()))
            for (trade, qty) in sortedTradeOptions:
                self.doStep('Sell {} x'.format(localedNo(qty)), trade.name(), '@ {}cr'.format(localedNo(trade.costCr + trade.gainCr)))
            print()

            gainCr += hop[1]
            if args.detail and gainCr > 0:
                self.note("GAINED: {}cr, CREDITS: {}cr".format(localedNo(gainCr), localedNo(credits + gainCr)))

            if hopNo < lastHopIdx:
                print("\n--------------------------------------\n")
            else:
                addOutput("</div>")
        if mfd:
            mfd.display('FINISHED', "+{}cr".format(localedNo(gainCr)), "={}cr".format(localedNo(credits + gainCr)))
            mfd.attention(3)
            time.sleep(1.5)


######################################################################
# "run" command functionality.

def parseAvoids(tdb, args):
    """
        Process a list of avoidances.
    """

    global avoidItems, avoidSystems, avoidStations

    avoidances = args.avoid

    # You can use --avoid to specify an item, system or station.
    # and you can group them together with commas or list them
    # individually.
    for avoid in ','.join(avoidances).split(','):
        # Is it an item?
        item, system, station = None, None, None
        try:
            item = tdb.lookupItem(avoid)
            avoidItems.append(item)
            if TradeDB.normalizedStr(item.name()) == TradeDB.normalizedStr(avoid):
                continue
        except LookupError:
            pass
        # Is it a system perhaps?
        try:
            system = tdb.lookupSystem(avoid)
            avoidSystems.append(system)
            if TradeDB.normalizedStr(system.str()) == TradeDB.normalizedStr(avoid):
                continue
        except LookupError:
            pass
        # Or perhaps it is a station
        try:
            station = tdb.lookupStationExplicitly(avoid)
            if (not system) or (station.system is not system):
                avoidSystems.append(station.system)
                avoidStations.append(station)
            if TradeDB.normalizedStr(station.str()) == TradeDB.normalizedStr(avoid):
                continue
        except LookupError as e:
            pass

        # If it was none of the above, whine about it
        if not (item or system or station):
            raise CommandLineError("Unknown item/system/station: %s" % avoid)

        # But if it matched more than once, whine about ambiguity
        if item and system: raise AmbiguityError('Avoidance', avoid, [ item, system.str() ])
        if item and station: raise AmbiguityError('Avoidance', avoid, [ item, station.str() ])
        if system and station and station.system != system: raise AmbiguityError('Avoidance', avoid, [ system.str(), station.str() ])

    if args.debug:
        print("Avoiding items %s, systems %s, stations %s" % (
            [ item.name() for item in avoidItems ],
            [ system.name() for system in avoidSystems ],
            [ station.name() for station in avoidStations ]
        ))


def parseVias(tdb, args):
    """
        Process a list of station names and build them into a
        list of waypoints for the route.
    """

    # accept [ "a", "b,c", "d" ] by joining everything and then splitting it.
    global viaStations

    for via in ",".join(args.via).split(","):
        station = tdb.lookupStation(via)
        if station.itemCount == 0:
            raise NoDataError("No price data available for via station {}.".format(station.name()))
        viaStations.add(station)


def processRunArguments(tdb, args):
    """
        Process arguments to the 'run' option.
    """

    global origins, originStation, finalStation, maxUnits, originName, destName, unspecifiedHops

    if args.credits < 0:
        raise CommandLineError("Invalid (negative) value for initial credits")
    # I'm going to allow 0 credits as a future way of saying "just fly"

    if args.routes < 1:
        raise CommandLineError("Maximum routes has to be 1 or higher")
    if args.routes > 1 and args.checklist:
        raise CommandLineError("Checklist can only be applied to a single route.")

    if args.hops < 1:
        raise CommandLineError("Minimum of 1 hop required")
    if args.hops > 64:
        raise CommandLineError("Too many hops without more optimization")

    if args.maxJumpsPer < 0:
        raise CommandLineError("Negative jumps: you're already there?")

    if args.origin:
        originName = args.origin
        originStation = tdb.lookupStation(originName)
        origins = [ originStation ]
    else:
        origins = [ station for station in tdb.stationByID.values() ]

    if args.dest:
        destName = args.dest
        finalStation = tdb.lookupStation(destName)
        if args.hops == 1 and originStation and finalStation and originStation == finalStation:
            raise CommandLineError("More than one hop required to use same from/to destination")

    if args.avoid:
        parseAvoids(tdb, args)
    if args.via:
        parseVias(tdb, args)

    unspecifiedHops = args.hops + (0 if originStation else 1) - (1 if finalStation else 0)
    if len(viaStations) > unspecifiedHops:
        raise CommandLineError("Too many vias: {} stations vs {} hops available.".format(len(viaStations), unspecifiedHops))

    # If the user specified a ship, use it to fill out details unless
    # the user has explicitly supplied them. E.g. if the user says
    # --ship sidewinder --capacity 2, use their capacity limit.
    if args.ship:
        ship = tdb.lookupShip(args.ship)
        args.ship = ship
        if args.capacity is None: args.capacity = ship.capacity
        if args.maxLyPer is None: args.maxLyPer = ship.maxLyFull
    if args.capacity is None:
        raise CommandLineError("Missing '--capacity' or '--ship' argument")
    if args.maxLyPer is None:
        raise CommandLineError("Missing '--ly-per' or '--ship' argument")
    if args.capacity < 0:
        raise CommandLineError("Invalid (negative) cargo capacity")
    if args.capacity > 1000:
        raise CommandLineError("Capacity > 1000 not supported (you specified %s)" % args.capacity)

    if args.limit and args.limit > args.capacity:
        raise CommandLineError("'limit' must be <= capacity")
    if args.limit and args.limit < 0:
        raise CommandLineError("'limit' can't be negative, silly")
    maxUnits = args.limit if args.limit else args.capacity

    arbitraryInsuranceBuffer = 42
    if args.insurance and args.insurance >= (args.credits + arbitraryInsuranceBuffer):
        raise CommandLineError("Insurance leaves no margin for trade")

    if args.unique and args.hops >= len(tdb.stationByID):
        raise CommandLineError("Requested unique trip with more hops than there are stations...")
    if args.unique:
        if ((originStation and originStation == finalStation) or
                 (originStation and originStation in viaStations) or
                 (finalStation and finalStation in viaStations)):
            raise CommandLineError("from/to/via repeat conflicts with --unique")

    tdb.loadTrades()

    if originStation and originStation.itemCount == 0:
        raise NoDataError("Start station {} doesn't have any price data.".format(originStation.name()))
    if finalStation and finalStation.itemCount == 0:
        raise NoDataError("End station {} doesn't have any price data.".format(finalStation.name()))
    if finalStation and args.hops == 1 and originStation and not finalStation in originStation.tradingWith:
        raise CommandLineError("No profitable items found between {} and {}".format(originStation.name(), finalStation.name()))
    if originStation and len(originStation.tradingWith) == 0:
        raise NoDataError("No data found for potential buyers for items from {}.".format(originStation.name()))

    if args.x52pro:
        from mfd import X52ProMFD
        args.mfd = X52ProMFD()
    else:
        args.mfd = None


def runCommand(tdb, args):
    """ Calculate trade runs. """

    if args.debug: print("# 'run' mode")

    if tdb.tradingCount == 0:
        raise NoDataError("Database does not contain any profitable trades.")

    processRunArguments(tdb, args)

    startCr = args.credits - args.insurance
    routes = [
        Route(stations=[src], hops=[], jumps=[], startCr=startCr, gainCr=0)
        for src in origins
        if not (src in avoidStations or src.system in avoidSystems)
    ]
    numHops = args.hops
    lastHop = numHops - 1
    viaStartPos = 1 if originStation else 0

    if args.debug or args.detail or args.webOutput:
        summaries = [ 'With {}cr'.format(localedNo(args.credits)) ]
        summaries += [ 'From {}'.format(originStation.str() if originStation else 'Anywhere') ]
        summaries += [ 'To {}'.format(finalStation.str() if finalStation else 'Anywhere') ]
        if viaStations: summaries += [ 'Via {}'.format(', '.join([ station.str() for station in viaStations ])) ]
        print(*summaries, sep=' / ')
        print("%d cap, %d hops, max %d jumps/hop and max %0.2f ly/jump" % (args.capacity, numHops, args.maxJumpsPer, args.maxLyPer))
        print()

    # Instantiate the calculator object
    calc = TradeCalc(tdb, debug=args.debug, capacity=args.capacity, maxUnits=maxUnits, margin=args.margin, unique=args.unique)

    # Build a single list of places we want to avoid
    # TODO: Keep these seperate because we wind up spending
    # time breaking the list down in getDestinations.
    avoidPlaces = avoidSystems + avoidStations

    if args.debug: print("unspecified hops {}, numHops {}, viaStations {}".format(unspecifiedHops, numHops, len(viaStations)))
    for hopNo in range(numHops):
        if calc.debug: print("# Hop %d" % hopNo)
        if args.mfd:
            args.mfd.display('TradeDangerous', 'CALCULATING', 'Hop {}'.format(hopNo))

        restrictTo = None
        if hopNo == lastHop and finalStation:
            restrictTo = set([finalStation])
            ### TODO:
            ### if hopsLeft < len(viaStations):
            ###   ... only keep routes that include len(viaStations)-hopsLeft routes
            ### Thus: If you specify 5 hops via a,b,c and we are on hop 3, only keep
            ### routes that include a, b or c. On hop 4, only include routes that
            ### already include 2 of the vias, on hop 5, require all 3.
            if viaStations:
                routes = [ route for route in routes if viaStations & set(route.route[viaStartPos:]) ]
        elif unspecifiedHops == len(viaStations):
            # Everywhere we're going is in the viaStations list.
            restrictTo = viaStations

        routes = calc.getBestHops(routes, startCr,
                                  restrictTo=restrictTo, avoidItems=avoidItems, avoidPlaces=avoidPlaces,
                                  maxJumpsPer=args.maxJumpsPer, maxLyPer=args.maxLyPer)

    if viaStations:
        routes = [ route for route in routes if viaStations & set(route.route[viaStartPos:]) ]

    if not routes:
        print("No profitable trades matched your critera, or price data along the route is missing.")
        if args.webOutput:
            addOutput("No profitable trades matched your critera, or price data along the route is missing.")
        return

    routes.sort()

    for i in range(0, min(len(routes), args.routes)):
        print(routes[i].detail(detail=args.detail))
        addOutput(routes[i].detail(detail=args.detail))        

    # User wants to be guided through the route.
    if args.checklist:
        assert args.routes == 1
        cl = Checklist(tdb, args.mfd)
        cl.run(routes[0], args.credits)


######################################################################
# "update" command functionality.

def getEditorPaths(args, editorName, envVar, windowsFolders, winExe, nixExe):
    if args.debug: print("# Locating {} editor".format(editorName))
    try:
        return os.environ[envVar]
    except KeyError: pass

    paths = []

    import platform
    system = platform.system()
    if system == 'Windows':
        binary = winExe
        for folder in ["Program Files", "Program Files (x86)"]:
            for version in windowsFolders:
                paths.append("{}\\{}\\{}".format(os.environ['SystemDrive'], folder, version))
    else:
        binary = nixExe

    try:
        paths += os.environ['PATH'].split(os.pathsep)
    except KeyError: pass

    for path in paths:
        candidate = os.path.join(path, binary)
        try:
            if pathlib.Path(candidate).exists():
                return candidate
        except OSError:
            pass

    raise CommandLineError("ERROR: Unable to locate {} editor.\nEither specify the path to your editor with --editor or set the {} environment variable to point to it.".format(editorName, envVar))


def editUpdate(tdb, args, stationID):
    """
        Dump the price data for a specific station to a file and
        launch the user's text editor to let them make changes
        to the file.

        If the user makes changes, re-load the file, update the
        database and regenerate the master .prices file.
    """

    if args.debug: print("# 'update' mode with editor. editor:{} station:{}".format(args.editor, args.station))

    import buildcache
    import prices
    import subprocess
    import os

    editor, editorArgs = args.editor, []
    if args.sublime:
        if args.debug: print("# Sublime mode")
        if not editor:
            editor = getEditorPaths(args, "sublime", "SUBLIME_EDITOR", ["Sublime Text 3", "Sublime Text 2"], "sublime_text.exe", "subl")
        editorArgs += [ "--wait" ]
    elif args.npp:
        if args.debug: print("# Notepad++ mode")
        if not editor:
            editor = getEditorPaths(args, "notepad++", "NOTEPADPP_EDITOR", ["Notepad++"], "notepad++.exe", "notepad++")
        if not args.quiet: print("NOTE: You'll need to exit Notepad++ to return control back to trade.py")
    elif args.vim:
        if args.debug: print("# VI iMproved mode")
        if not editor:
            # Hack... Hackity hack, hack, hack.
            # This has a disadvantage in that: we don't check for just "vim" (no .exe) under Windows
            vimDirs = [ "Git\\share\\vim\\vim{}".format(vimVer) for vimVer in range(70,75) ]
            editor = getEditorPaths(args, "vim", "EDITOR", vimDirs, "vim.exe", "vim")
    elif args.notepad:
        if args.debug: print("# Notepad mode")
        editor = "notepad.exe"  # herp

    try:
        envArgs = os.environ["EDITOR_ARGS"]
        if envArgs: editorArgs += envArgs.split(' ')
    except KeyError: pass

    # Create a temporary text file with a list of the price data.
    tmpPath = pathlib.Path("prices.tmp")
    if tmpPath.exists():
        print("ERROR: Temporary file ({}) already exists.".format(tmpPath))
        sys.exit(1)
    absoluteFilename = None
    try:
        elementMask = prices.Element.basic
        if args.supply: elementMask |= prices.Element.supply
        if args.timestamps: elementMask |= prices.Element.timestamp
        # Open the file and dump data to it.
        with tmpPath.open("w") as tmpFile:
            # Remember the filename so we know we need to delete it.
            absoluteFilename = str(tmpPath.resolve())
            prices.dumpPrices(args.db, elementMask, file=tmpFile, stationID=stationID, defaultZero=args.forceNa, debug=args.debug)

        # Stat the file so we can determine if the user writes to it.
        # Use the most recent create/modified timestamp.
        preStat = tmpPath.stat()
        preStamp = max(preStat.st_mtime, preStat.st_ctime)

        # Launch the editor
        editorCommandLine = [ editor ] + editorArgs + [ absoluteFilename ]
        if args.debug: print("# Invoking [{}]".format(' '.join(editorCommandLine)))
        try:
            result = subprocess.call(editorCommandLine)
        except FileNotFoundError:
            raise CommandLineError("Unable to launch specified editor: {}".format(editorCommandLine))
        if result != 0:
            print("NOTE: Edit failed ({}), nothing to import.".format(result))
            sys.exit(1)

        # Did they update the file? Some editors destroy the file and rewrite it,
        # other files just write back to it, and some OSes do weird things with
        # these timestamps. That's why we have to use both mtime and ctime.
        postStat = tmpPath.stat()
        postStamp = max(postStat.st_mtime, postStat.st_ctime)

        if postStamp == preStamp:
            import random
            print("- No changes detected - doing nothing. {}".format(random.choice([
                    "Brilliant!", "I'll get my coat.", "I ain't seen you.", "You ain't seen me", "... which was nice", "Bingo!", "Scorchio!", "Boutros, boutros, ghali!", "I'm Ed Winchester!", "Suit you, sir! Oh!"
                ])))
            sys.exit(0)

        if args.debug:
            print("# File changed - importing data.")

        buildcache.processPricesFile(db=tdb.getDB(), pricesPath=tmpPath, stationID=stationID, defaultZero=args.forceNa, debug=args.debug)

        # If everything worked, we need to re-build the prices file.
        if args.debug:
            print("# Update complete, regenerating .prices file")

        with tdb.pricesPath.open("w") as pricesFile:
            prices.dumpPrices(args.db, prices.Element.full, file=pricesFile, debug=args.debug)
        # Update the DB file so we don't regenerate it.
        pathlib.Path(args.db).touch()

    finally:
        # If we created the file, we delete the file.
        if absoluteFilename: tmpPath.unlink()


def updateCommand(tdb, args):
    """
        Allow the user to update the prices database.
    """

    station = tdb.lookupStation(args.station)
    stationID = station.ID

    if args.all or args.zero:
        raise CommandLineError("--all and --zero have been removed. Use '--supply' (-S for short) if you want to edit demand and stock values during update. Use '--timestamps' (-T for short) if you want to include timestamps.")

    if args._editing:
        # User specified one of the options to use an editor.
        return editUpdate(tdb, args, stationID)

    if args.debug: print('# guided "update" mode station:{}'.format(args.station))

    print("Guided mode not implemented yet. Try using --editor, --sublime or --notepad")


######################################################################
#

def lookupSystemByNameOrStation(tdb, name, intent):
    """
        Look up a name using either a system or station name.
    """

    try:
        return tdb.lookupSystem(name)
    except LookupError:
        try:
            return tdb.lookupStationExplicitly(name).system
        except LookupError:
            raise CommandLineError("Unknown {} system/station, '{}'".format(intent, name))


def distanceAlongPill(tdb, sc, percent):
    """
        Estimate a distance along the Pill using 2 reference systems
    """
    sa = tdb.lookupSystem("Eranin")
    sb = tdb.lookupSystem("HIP 107457")
    dotProduct = (sb.posX-sa.posX) * (sc.posX-sa.posX) \
               + (sb.posY-sa.posY) * (sc.posY-sa.posY) \
               + (sb.posZ-sa.posZ) * (sc.posZ-sa.posZ)
    length = math.sqrt((sb.posX-sa.posX) * (sb.posX-sa.posX)
                     + (sb.posY-sa.posY) * (sb.posY-sa.posY)
                     + (sb.posZ-sa.posZ) * (sb.posZ-sa.posZ))
    if percent:
        return 100. * dotProduct / length / length

    return dotProduct / length


def localCommand(tdb, args):
    """
        Local systems
    """

    srcSystem = lookupSystemByNameOrStation(tdb, args.system, 'system')

    if args.ship:
        ship = tdb.lookupShip(args.ship)
        args.ship = ship
        if args.ly is None: args.ly = (ship.maxLyFull if args.full else ship.maxLyEmpty)
    ly = args.ly or tdb.maxSystemLinkLy

    tdb.buildLinks()

    printHeading("Local systems to {} within {} ly.".format(srcSystem.name(), ly))

    distances = { }

    for (destSys, destDist) in srcSystem.links.items():
        if args.debug:
            print("Checking {} dist={:5.2f}".format(destSys.str(), destDist))
        if destDist > ly:
            continue
        distances[destSys] = destDist

    for (system, dist) in sorted(distances.items(), key=lambda x: x[1]):
        pillLength = ""
        if args.pill or args.percent:
            pillLengthFormat = " [{:4.0f}%]" if args.percent else " [{:5.1f}]"
            pillLength = pillLengthFormat.format(distanceAlongPill(tdb, system, args.percent))
        print("{:5.2f}{} {}".format(dist, pillLength, system.str()))
        if args.detail:
            for (station) in system.stations:
                stationDistance = " {} ls".format(station.lsFromStar) if station.lsFromStar > 0 else ""
                print("\t<{}>{}".format(station.str(), stationDistance))


def navCommand(tdb, args):
    """
        Give player directions A->B
    """

    srcSystem = lookupSystemByNameOrStation(tdb, args.start, 'start')
    dstSystem = lookupSystemByNameOrStation(tdb, args.end, 'end')

    avoiding = []
    if args.ship:
        ship = tdb.lookupShip(args.ship)
        args.ship = ship
        if args.maxLyPer is None: args.maxLyPer = (ship.maxLyFull if args.full else ship.maxLyEmpty)
    maxLyPer = args.maxLyPer or tdb.maxSystemLinkLy

    if args.debug:
        print("# Route from {} to {} with max {} ly per jump.".format(srcSystem.name(), dstSystem.name(), maxLyPer))

    openList = { srcSystem: 0.0 }
    distances = { srcSystem: [ 0.0, None ] }

    tdb.buildLinks()

    # As long as the open list is not empty, keep iterating.
    while openList and not dstSystem in distances:
        # Expand the search domain by one jump; grab the list of
        # nodes that are this many hops out and then clear the list.
        openNodes, openList = openList, {}

        for (node, startDist) in openNodes.items():
            for (destSys, destDist) in node.links.items():
                if destDist > maxLyPer:
                    continue
                dist = startDist + destDist
                # If we already have a shorter path, do nothing
                try:
                    distNode = distances[destSys]
                    if distNode[0] <= dist:
                        continue
                    distNode[0], distNode[1] = dist, node
                except KeyError:
                    distances[destSys] = [ dist, node ]
                assert not destSys in openList or openList[destSys] > dist
                openList[destSys] = dist

    # Unravel the route by tracing back the vias.
    route = [ dstSystem ]
    try:
        while route[-1] != srcSystem:
            jumpEnd = route[-1]
            jumpStart = distances[jumpEnd][1]
            route.append(jumpStart)
    except KeyError:
        print("No route found between {} and {} with {}ly jump limit.".format(srcSystem.name(), dstSystem.name(), maxLyPer))
        return
    route.reverse()
    titleFormat = "From {src} to {dst} with {mly}ly per jump limit."
    if args.webOutput:
#        labelFormat = "{act:<6} | {sys:<30}"
        stepFormat = "<td>{act:<6}</td><td>{sys}</td><td>{jly:>7.2f}</td><td>{tly:>8.2f}</td>"
        labelFormat = "<thead><th>{act}</th><th>{sys}</th><th>{jly}</th><th>{tly}</th></tr></thead><tbody>"    
    elif args.detail:
        labelFormat = "{act:<6} | {sys:<30} | {jly:<7} | {tly:<8}"
        stepFormat = "{act:<6} | {sys:<30} | {jly:>7.2f} | {tly:>8.2f}"
    elif not args.quiet:
        labelFormat = "{sys:<30} ({jly:<7})"
        stepFormat  = "{sys:<30} ({jly:>7.2f})"
    elif args.quiet == 1:
        titleFormat = "{src}->{dst} limit {mly}ly:"
        labelFormat = None
        stepFormat = " {sys}"
    else:
        titleFormat, labelFormat, stepFormat = None, None, "{sys}"

    if titleFormat:
        print(titleFormat.format(src=srcSystem.name(), dst=dstSystem.name(), mly=maxLyPer))
        if args.webOutput:
            addOutput(titleFormat.format(src=srcSystem.name(), dst=dstSystem.name(), mly=maxLyPer))

    if labelFormat:
        printHeading(labelFormat.format(act='Action', sys='System', jly='Jump Ly', tly='Total Ly'))
        if args.webOutput:
            addOutput(labelFormat.format(act='Action', sys='System', jly='Jump Ly', tly='Total Ly'))

    lastHop, totalLy = None, 0.00
    def present(action, system):
        nonlocal lastHop, totalLy
        jumpLy = system.links[lastHop] if lastHop else 0.00
        totalLy += jumpLy
        print(stepFormat.format(act=action, sys=system.name(), jly=jumpLy, tly=totalLy))
        if args.webOutput:
            addOutput("<tr>" + stepFormat.format(act=action, sys=system.name(), jly=jumpLy, tly=totalLy) + "<tr>")
        lastHop = system

    present('Depart', srcSystem)
    for viaSys in route[1:-1]:
        present('Via', viaSys)
    present('Arrive', dstSystem)


######################################################################
# 

def buyCommand(tdb, args):
    """
        Locate places selling a given item.
    """

    item = tdb.lookupItem(args.item)

    # Constraints
    constraints = [ "(item_id = ?)", "buy_from > 0", "stock != 0" ]
    bindValues = [ item.ID ]

    if args.quantity:
        constraints.append("(stock = -1 or stock >= ?)")
        bindValues.append(args.quantity)

    near = args.near
    if near:
        tdb.buildLinks()
        nearSystem = tdb.lookupSystem(near)
        maxLy = float("inf") if args.maxLyPer is None else args.maxLyPer
        # Uh - why haven't I made a function on System to get a
        # list of all the systems within N hops at L ly per hop?
        stations = []
        for station in nearSystem.stations:
            if station.itemCount > 0:
                stations.append(str(station.ID))
        for system, dist in nearSystem.links.items():
            if dist <= maxLy:
                for station in system.stations:
                    if station.itemCount > 0:
                        stations.append(str(station.ID))
        if not stations:
            raise NoDataError("No stations listed as selling items within range")
        constraints.append("station_id IN ({})".format(','.join(stations)))

    whereClause = ' AND '.join(constraints)
    stmt = """
                SELECT station_id, buy_from, stock
                  FROM Price
                 WHERE {}
            """.format(whereClause)
    if args.debug:
        print("* SQL: {}".format(stmt))
    cur = tdb.query(stmt, bindValues)

    from collections import namedtuple
    Result = namedtuple('Result', [ 'station', 'cost', 'stock', 'dist' ])
    results = []
    stationByID = tdb.stationByID
    dist = 0.0
    for (stationID, costCr, stock) in cur:
        stn = stationByID[stationID]
        if near:
            dist = stn.system.links[nearSystem] if stn.system != nearSystem else 0.0
        results.append(Result(stationByID[stationID], costCr, stock, dist))

    if not results:
        raise NoDataError("No available items found")

    if args.sortByStock:
        results.sort(key=lambda result: result.cost)
        results.sort(key=lambda result: result.stock, reverse=True)
    else:
        results.sort(key=lambda result: result.stock, reverse=True)
        results.sort(key=lambda result: result.cost)
        if near and not args.sortByPrice:
            results.sort(key=lambda result: result.dist)

    maxStnNameLen = len(max(results, key=lambda result: len(result.station.dbname) + len(result.station.system.dbname) + 1).station.name())
    printHeading("{:<{maxStnLen}} {:>10} {:>10} {:{distFmt}}".format(
            "Station", "Cost", "Stock", "Ly" if near else "",
            maxStnLen=maxStnNameLen,
            distFmt=">6" if near else ""
        ))
    if args.webOutput:
        addOutput("<thead><tr><th>Station</th><th>Cost</th><th>Stock</th><th>Distance</th></tr></thead><tbody>")
        
    for result in results:
        print("{:<{maxStnLen}} {:>10n} {:>10} {:{distFmt}}".format(
                result.station.name(),
                result.cost,
                localedNo(result.stock) if result.stock > 0 else "",
                result.dist if near else "",
                maxStnLen=maxStnNameLen,
                distFmt=">6.2f" if near else ""
            ))
        if args.webOutput:
            addOutput("<tr><td><a href='#run' h='mode' onclick='$(\"#toField\").val(\"{}\")'>{:<{maxStnLen}}</a></td><td>{:>10n}</td><td>{:>10}</td><td>{:{distFmt}}</td></tr>".format(
                result.station.dbname,
                result.station.name(),
                result.cost,
                localedNo(result.stock) if result.stock > 0 else "",
                result.dist if near else "",
                maxStnLen=maxStnNameLen,
                distFmt=">6.2f" if near else ""
            ))           
        

######################################################################
# main entry point


def main():
    global args

    parser = argparse.ArgumentParser(description='Trade run calculator', add_help=False, epilog='For help on a specific command, use the command followed by -h.')
    parser.set_defaults(_editing=False)

    # Arguments common to all subparsers.
    commonArgs = parser.add_argument_group('Common Switches')
    commonArgs.add_argument('-h', '--help', help='Show this help message and exit.', action=HelpAction, nargs=0)
    commonArgs.add_argument('--debug',  '-w', help='Enable diagnostic output.', default=0, required=False, action='count')
    commonArgs.add_argument('--detail', '-v', help='Increase level  of detail in output.', default=0, required=False, action='count')
    commonArgs.add_argument('--quiet',  '-q', help='Reduce level of detail in output.', default=0, required=False, action='count')
    commonArgs.add_argument('--db', help='Specify location of the SQLite database. Default: {}'.format(TradeDB.defaultDB), type=str, default=str(TradeDB.defaultDB))
    commonArgs.add_argument('--cwd',    '-C', help='Change the directory relative to which TradeDangerous will try to access files such as the .db, etc.', type=str, required=False)

    subparsers = parser.add_subparsers(dest='subparser', title='Commands')

    # Find places that are selling an item within range of a specified system.
    buyParser = makeSubParser(subparsers, 'buy', 'Find places to buy a given item within range of a given station.', buyCommand,
        arguments = [
            ParseArgument('item', help='Name of item to query.', type=str),
        ],
        switches = [
            ParseArgument('--quantity', help='Require at least this quantity.', type=int, default=0),
            ParseArgument('--near', help='Find sellers within jump range of this system.', type=str),
            ParseArgument('--ly-per', help='Maximum light years per jump.', metavar='N.NN', dest='maxLyPer', type=float, default=None),
            ParseArgument('--webOutput', help='Output to Flask instead of console',action='store_true', default=False),                        
            [
                ParseArgument('--price-sort', '-P', help='(When using --near) Sort by price not distance', dest='sortByPrice', action='store_true', default=False),
                ParseArgument('--stock-sort', '-S', help='Sort by stock followed by price', dest='sortByStock', action='store_true', default=False),
            ],
        ],
    )

    # "nav" tells you how to get from one place to another.
    navParser = makeSubParser(subparsers, 'nav', 'Calculate a route between two systems.', navCommand,
        arguments = [
            ParseArgument('start', help='System to start from', type=str),
            ParseArgument('end', help='System to end at', type=str),
        ],
        switches = [
            ParseArgument('--ship', help='Use the maximum jump distance of the specified ship (defaults to the empty value).', metavar='shiptype', type=str),
            ParseArgument('--full', help='(With --ship) Limits the jump distance to that of a full ship.', action='store_true', default=False),
            ParseArgument('--ly-per', help='Maximum light years per jump.', metavar='N.NN', type=float, dest='maxLyPer'),
            ParseArgument('--webOutput', help='Output to Flask instead of console',action='store_true', default=False),            
        ]
    )

    # "local" shows systems local to given system.
    localParser = makeSubParser(subparsers, 'local', 'Calculate local systems.', localCommand,
        arguments = [
            ParseArgument('system', help='System to measure from', type=str),
        ],
        switches = [
            ParseArgument('--ship', help='Use the maximum jump distance of the specified ship (defaults to the empty value).', metavar='shiptype', type=str),
            ParseArgument('--full', help='(With --ship) Limits the jump distance to that of a full ship.', action='store_true', default=False),
            ParseArgument('--ly', help='Maximum light years to measure.', metavar='N.NN', type=float, dest='ly'),
            [
              ParseArgument('--pill', help='Show distance along the pill in ly.', action='store_true', default=False),
              ParseArgument('--percent', help='Show distance along pill as percentage.', action='store_true', default=False),
            ],
       ]
    )

    # "run" calculates a trade run.
    runParser = makeSubParser(subparsers, 'run', 'Calculate best trade run.', runCommand,
        arguments = [
            ParseArgument('--credits', help='Starting credits.', metavar='CR', type=int)
        ],
        switches = [
            ParseArgument('--ship', help='Set capacity and ly-per from ship type.', metavar='shiptype', type=str),
            ParseArgument('--capacity', help='Maximum capacity of cargo hold.', metavar='N', type=int),
            ParseArgument('--from', help='Starting system/station.', metavar='STATION', dest='origin'),
            ParseArgument('--to', help='Final system/station.', metavar='STATION', dest='dest'),
            ParseArgument('--via', help='Require specified systems/stations to be en-route.', metavar='PLACE[,PLACE,...]', action='append'),
            ParseArgument('--avoid', help='Exclude an item, system or station from trading. Partial matches allowed, e.g. "dom.App" or "domap" matches "Dom. Appliances".', action='append'),
            ParseArgument('--hops', help='Number of hops (station-to-station) to run.', metavar='N', type=int, default=2),
            ParseArgument('--jumps-per', help='Maximum number of jumps (system-to-system) per hop.', metavar='N', dest='maxJumpsPer', type=int, default=2),
            ParseArgument('--ly-per', help='Maximum light years per jump.', metavar='N.NN', type=float, dest='maxLyPer'),
            ParseArgument('--limit', help='Maximum units of any one cargo item to buy (0: unlimited).', metavar='N', type=int),
            ParseArgument('--unique', help='Only visit each station once.', action='store_true', default=False),
            ParseArgument('--margin', help='Reduce gains made on each hop to provide a margin of error for market fluctuations (e.g: 0.25 reduces gains by 1/4). 0<: N<: 0.25.', metavar='N.NN', type=float, default=0.00),
            ParseArgument('--insurance', help='Reserve at least this many credits to cover insurance.', metavar='CR', type=int, default=0),
            ParseArgument('--routes', help='Maximum number of routes to show. DEFAULT: 1', metavar='N', type=int, default=1),
            ParseArgument('--checklist', help='Provide a checklist flow for the route.', action='store_true', default=False),
            ParseArgument('--x52-pro', help='Enable experimental X52 Pro MFD output.', action='store_true', dest='x52pro', default=False),
            ParseArgument('--webOutput', help='Output to Flask instead of console',action='store_true', default=False),
        ]
    )

    # "update" provides the user a way to edit prices.
    updateParser = makeSubParser(subparsers, 'update', 'Update prices for a station.', updateCommand,
        epilog="Generates a human-readable version of the price list for a given station and opens it in the specified text editor.\n"
            "The format is intended to closely resemble the presentation of the market in-game. If you change the order items are listed in, "
            "the order will be kept for future edits, making it easier to quickly check for changes.",
        arguments = [
            ParseArgument('station', help='Name of the station to update.', type=str)
        ],
        switches = [
            ParseArgument('--editor', help='Generates a text file containing the prices for the station and loads it into the specified editor.', default=None, type=str, action=EditAction),
            ParseArgument('--all', help='DEPRECATED - See --supply and --timestamps instead.', action='store_true', default=False),
            ParseArgument('--zero', help='DEPRECATED - See --force-na instead.', action='store_true', default=False),
            ParseArgument('--supply', '-S', help='Includes demand and stock (supply) values in the update.', action='store_true', default=False),
            ParseArgument('--timestamps', '-T', help='Includes timestamps in the update.', action='store_true', default=False),
            ParseArgument('--force-na', '-0', help="Forces 'unk' supply to become 'n/a' by default", action='store_true', default=False, dest='forceNa'),
            [   # Mutually exclusive group:
                ParseArgument('--sublime', help='Like --editor but uses Sublime Text (2 or 3), which is nice.', action=EditActionStoreTrue),
                ParseArgument('--notepad', help='Like --editor but uses Notepad.', action=EditActionStoreTrue),
                ParseArgument('--npp',     help='Like --editor but uses Notepad++.', action=EditActionStoreTrue),
                ParseArgument('--vim',     help='Like --editor but uses vim.', action=EditActionStoreTrue),
            ]
        ]
    )

    args = parser.parse_args(args)
    if not 'proc' in args:
        helpText = "No sub-command specified.\n" + parser.format_help() + "\nNote: As of v3 you need to specify one of the 'sub-commands' listed above (run, nav, etc)."
        raise CommandLineError(helpText)

    if args.detail and args.quiet:
        raise CommandLineError("'--detail' (-v) and '--quiet' (-q) are mutually exclusive.")

    # If a directory was specified, relocate to it.
    # Otherwise, try to chdir to 
    if args.cwd:
        os.chdir(args.cwd)
    else:
        if sys.argv[0]:
            cwdPath = pathlib.Path('.').resolve()
            exePath = pathlib.Path(sys.argv[0]).parent.resolve()
            if cwdPath != exePath:
                if args.debug: print("# cwd at launch was: {}, changing to {} to match trade.py".format(cwdPath, exePath))
                os.chdir(str(exePath))

    # load the database
    tdb = TradeDB(debug=args.debug, dbFilename=args.db, buildLinks=False, includeTrades=False)

    # run the commands
    commandFunction = args.proc
    return commandFunction(tdb, args)


######################################################################


if __name__ == "__main__":
    try:
        main()
    except (TradeException) as e:
        print("%s: %s" % (sys.argv[0], str(e)))
