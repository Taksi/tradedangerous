#!/usr/bin/env python
######################################################################
# Imports
from flask import Flask, request, url_for,redirect,jsonify
import time
import json
import os
import pathlib 
import buildcache
import trade
from tradedb import TradeDB, AmbiguityError
from tradeexcept import TradeException
######################################################################
# Classes

class LookupError(TradeException):
    """
       can't look it up
    """
    def __init__(self, errorStr):
        self.errorStr = errorStr
    def __str__(self):
        addOutput(self.errorStr)    
        return "Error: {}\n".format(self.errorStr)  
    pass

######################################################################

app = Flask(__name__,static_url_path='')

@app.route('/')
def hi():
    return app.send_static_file("index.htm")

@app.route('/doRun/', methods=['POST'])
def getRun():
    #there has to be a better way of doing this, embarrassing but working
    trade.tdb = TradeDB()
    trade.originStation, trade.finalStation = None, None
    trade.avoidItems, trade.avoidSystems, trade.avoidStations = [], [], []
    trade.viaStations = set()
    trade.originName, trade.destName = "Any", "Any"
    trade.origins = []
    trade.maxUnits = 0
    
    args = request.form["command"].split()

    trade.args = args
    trade.tradeOutput = ""
    try:
        trade.main()
#    except (Exception) as e:
#        trade.addOutput(str(e))
    finally:
        return trade.tradeMessage()


@app.route('/route/', methods=['POST'])
def getRoute():
    import trade
    trade.args = request.form["command"].split()
    trade.newResponse()
    try:
        trade.main()
    except (Exception) as e:
        trade.addOutput(str(e))
    finally:
        return jsonify(trade.getResponse())


@app.route('/prices/')
def prices():
    # load the database
    tdb = TradeDB()
    station = tdb.lookupStation(request.args.get("station"))
    stationID = station.ID
    tmpPath = pathlib.Path("static/prices.json")
    import prices
    try:
        with tmpPath.open("w") as tmpFile:
#            prices.dumpPrices(dbFilename=tdb.dbURI, withModified=True, stationID=stationID, file=tmpFile, defaultZero=False, debug=1, useJSON=True)
            prices.dumpPrices(tdb.dbURI, prices.Element.full, file=tmpFile, stationID=stationID, defaultZero=False, debug=0,useJSON=True)
    finally:
        return app.send_static_file("prices.json")

@app.route('/savePrices/', methods=['POST'])
def savePrices():
    #this should write changes to disk, or pass the JSON to another function
    return "Data updated for {}.".format(request.json['station'])

@app.route('/stations/')
def stationList():
    # load the database
    tdb = TradeDB()
    tdb._loadStations()
    resp = {"stationNames":[]}
    for station in tdb.stations():
        resp['stationNames'].append({"label":"{}/{}".format(station.system.dbname,station.dbname),"stationName":station.dbname,"itemCount":station.itemCount})
    return jsonify(resp)


@app.route('/station/')
def getStation():
    # load the database
    tdb = TradeDB()
    tdb._loadStations()
    resp = {"station":tdb.lookupStation(request.args.get('station', ''))}
    return jsonify(resp)


@app.route('/savePriceLines/', methods=['POST'])
def savePriceLines():
    tmpPath = pathlib.Path("prices.tmp")
    absoluteFilename = None
    # load the database
    tdb = TradeDB()    
    try:
        # Open the file and dump data to it.
        with tmpPath.open("w") as tmpFile:
            absoluteFilename = str(tmpPath.resolve())        
            tmpFile.write(request.form["lines"])
        station = tdb.lookupStation(request.form["station"])
        stationID = station.ID
        buildcache.processPricesFile(db=tdb.getDB(), pricesPath=tmpPath, stationID=stationID,debug=True)
        print("processed")
        import prices
        with tdb.pricesPath.open("w") as pricesFile:
            prices.dumpPrices(tdb.dbURI, prices.Element.full, file=pricesFile)
#        # Update the DB file so we don't regenerate it.
        pathlib.Path(tdb.dbURI).touch()

    finally:
#        if absoluteFilename: tmpPath.unlink()
        if absoluteFilename:
            os.rename(absoluteFilename, "prices.{}.{}.tmp".format(stationID,time.time()))
        return "Data updated for {}.".format(request.form['station'])

@app.errorhandler(TradeException)
def throwError(e):
    print("trade exception")
    return str(e)

if __name__ == '__main__':
  app.run( 
        host="0.0.0.0",
        port=int("3300"),
        debug=1
  )